# README #

- Modify v2ray/config.json if necessary (don't forget the client side as well)

### Update

```
./run pull
./run up -d
```

### Restart

```
./run restart
```

### View logs

```
./run logs -f
```
